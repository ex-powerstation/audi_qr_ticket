<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    protected $table = 'attempts';

    protected $fillable = ['code'];

    public function countAttemps($code)
    {
        return $this->where('code', $code)->count();
    }
}
