<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $table = 'participants';
    protected $connection = 'mysql_publicis';

    public function __toString()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function timeslot()
    {
        return $this->hasOne(\App\Models\Timeslot::class, 'id', 'time_slot_id');
    }

    public function code()
    {
        return $this->hasOne(\App\Models\Code::class, 'participant_id');
    }
}
