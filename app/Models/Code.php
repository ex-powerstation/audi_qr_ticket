<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'codes';

    protected $fillable = ['code', 'participant_id'];

    public function __toString()
    {
        return $this->code;
    }

    public function scopeAvailable($query)
    {
        $query->whereNull('participant_id');
    }

    public function scopeAttached($query)
    {
        $query->whereNotNull('participant_id');
    }

    public function participant()
    {
        return $this->belongsTo(\App\Models\Participant::class, 'participant_id');
    }

    public function attempts()
    {
        return $this->hasMany(\App\Models\Attempt::class, 'code', 'code');
    }

}
