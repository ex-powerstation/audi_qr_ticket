<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    protected $table = 'time_slots';
    protected $connection = 'mysql_publicis';

    public function __toString()
    {
        return $this->date . ', ' . $this->slot_from . ' to ' . $this->slot_to;
    }
}
