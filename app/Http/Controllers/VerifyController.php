<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Participant;
use Input;
use Redirect;

class VerifyController extends Controller
{
    public function verify()
    {
        return view('verify');
    }

    public function verify_get()
    {
        // Redirect back with error if no input is found
        if (!Input::has('credential')) {
            return back()->withErrors('*Error: Please insert Email or IC Number');
        }

        $credential = Input::get('credential');

        // Query Email & NRIC with input
        $participant = Participant::where(function ($q) use ($credential) {
            $q->where('email', $credential)
            ->orWhere('nric', $credential);
        })->first();

        // Redirect back with error if participant not found
        if (!$participant) {

            return back()->withErrors('*Error: Invalid Email or IC Number');

        } else {

            // Attach the participant to a QR code if haven't
            if (!$participant->code) {
                $participant->code()->save(Code::available()->first());
            }

            return Redirect::route('verify.info', $participant->id);

        }
    }

    public function info(Participant $participant)
    {
        return view('info', compact('participant'));
    }

    public function verify_email()
    {
        return back();
    }

    public function verify_print()
    {
        return back();
    }
}
