<?php

namespace App\Http\Controllers;

use App\Models\Attempt;
use App\Models\Code;

class EntryController extends Controller
{
    public function entry($qrcode = null)
    {
        $code = null;

        if ($qrcode) {
            $code = Code::where('code', $qrcode)->first();

            $attemp = new Attempt();
            $attemp->code = $code;
            $attemp->save();
        }

        return view('entry', compact(['code']));
    }

}
