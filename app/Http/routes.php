<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'verify', 'as' => 'verify'], function () {
    Route::get('/', ['as' => '', 'uses' => 'VerifyController@verify']);
    Route::get('/verify_get', ['as' => '.verify_get', 'uses' => 'VerifyController@verify_get']);
    Route::get('/info/{participant}', ['as' => '.info', 'uses' => 'VerifyController@info']);

    Route::get('/email', ['as' => '.email', 'uses' => 'VerifyController@email']);
    Route::get('/print', ['as' => '.print', 'uses' => 'VerifyController@print']);
});

Route::get('/entry/{code?}', ['as' => 'entry', 'uses' => 'EntryController@entry']);
