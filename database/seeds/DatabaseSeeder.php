<?php

use App\Models\Code;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Pre-generate 4000 QR codes
        factory(Code::class, 4000)->create();

        Model::reguard();
    }
}
