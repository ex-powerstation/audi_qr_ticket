<?php

use Illuminate\Database\Migrations\Migration;

class QrCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function ($table) {
            $table->increments('id');
            $table->string('code')->index();
            $table->string('participant_id')->index()->nullable();
            $table->timestamps();
        });

        Schema::create('attempts', function ($table) {
            $table->increments('id');
            $table->string('code')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('codes');
        Schema::drop('attempts');
    }
}
