<?php

$factory->define(App\Models\Code::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->unique()->ean13,
    ];
});
