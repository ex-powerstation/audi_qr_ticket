@extends('default')

@section('head_scripts')
  <link rel="stylesheet" type="text/css" href="/assets/main.css">
@show

@section('body')

  @if($code && $code->count())
    <div id="entry-overlay-container">
      (overlay)
      </br>
      @if($code->participant_id)
          count: {!! $code->attempts()->count() !!}
          </br>
          QR code: {!! $code !!}
          </br>
          Name: {!! $code->participant !!}
          </br>
          IC Number: {!! $code->participant->nric !!}
          </br>
          Email Address: {!! $code->participant->email !!}
          </br>
          Date & Time Slot: {!! $code->participant->timeslot !!}
          </br>
          Number of Guest: {!! $code->participant->total_no_pax !!}
          </br>
      @endif
    </div>
  @endif

  {!! Form::open(['route' => 'entry', 'method' => 'GET']) !!}

    {!! Form::text('code', null, ['placeholder' => 'ENTER UNIQUE CODE', 'autofocus', 'onblur' => "this.focus()"]) !!}

    {!! Form::submit('Check') !!}

  {!! Form::close() !!}

  @if($errors->any())
    <h4>{{$errors->first()}}</h4>
  @endif

@stop

@section('foot_scripts')
  <script src="/assets/bower/jquery/dist/jquery.min.js"></script>
  <script src="/assets/main.js"></script>
@show
