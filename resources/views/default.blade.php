<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    @section('head_title')
    @show
  </title>
  @section('head_scripts')
  @show
</head>
<body>
  @section('body')
  @show
  @section('footer')
  @show
  @section('foot_scripts')
  @show
</body>
</html>
