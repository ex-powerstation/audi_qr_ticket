@extends('default')

@section('body')

  {!! Form::open(['route' => 'verify.verify_get', 'method' => 'GET']) !!}

    {!! Form::text('credential', null, ['placeholder' => 'ENTER EMAIL OR IC NUMBER']) !!}

    {!! Form::submit('Verify') !!}

  {!! Form::close() !!}

  @if($errors->any())
    <h4>{{$errors->first()}}</h4>
  @endif

@stop
