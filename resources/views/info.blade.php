@extends('default')

@section('body')

  QR code: {!! $participant->code !!}
  </br>
  Name: {!! $participant !!}
  </br>
  IC Number: {!! $participant->nric !!}
  </br>
  Email Address: {!! $participant->email !!}
  </br>
  Date & Time Slot: {!! $participant->timeslot !!}
  </br>
  Number of Guest: {!! $participant->total_no_pax !!}
  </br>
  {!! Form::submit('Email QR') !!}
  {!! Form::submit('Print QR') !!}
@stop
